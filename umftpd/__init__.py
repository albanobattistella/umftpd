"""umftpd - Usermode FTP Server."""

import concurrent.futures
import datetime
import functools

import logging
import os
import pathlib
import re
import time
import typing

import umftpd.config as cfg
import umftpd.crypto as crypto
import umftpd.server as srv
import umftpd.server.ftp as ftp
import umftpd.server.ssh as ssh
import umftpd.ui as ui
import umftpd.uitk as uitk

from gi.repository import Adw, Gdk, GLib, Gtk


__author__ = 'Felipe A Hernandez <ergoithz@gmail.com>'


logger = logging.getLogger(__name__)


RE_MARKUP = re.compile('<[^>]+>')


class GLibLogHandler(logging.Handler):
    def __init__(self, callback):
        super().__init__()
        self.callback = callback

    def emit(self, record):
        GLib.idle_add(self.callback, record)


class Application(uitk.UiApplication):

    application_id = 'eu.ithz.umftpd'

    window: typing.Optional[Adw.ApplicationWindow] = None
    window_size_request: typing.Optional[tuple[int, int]] = None

    initialized: bool = False
    port_valid: bool = True
    port_wait: tuple[int, float] = 0, 0

    server_loop: typing.Optional[concurrent.futures.Future] = None
    server_loop_run: bool = False
    server_loop_interval: float = 0.5

    status_time: float = 0

    @property
    def last_port_wait(self):
        previous_timestamp = (
            self.port_wait == (0, 0)
            and self.config.info
            and self.config.info.last
            and self.config.info.last.timestamp()
            )
        return (
            (self.config.port, previous_timestamp + srv.time_wait())
            if previous_timestamp else
            self.port_wait
            )

    @property
    def server_loop_running(self) -> bool:
        return self.server_loop and self.server_loop.running()

    @property
    def server_class(self) -> typing.Type[srv.Server]:
        return ssh.SFTPServer if self.config.secure == 2 else ftp.FTPServer

    @property
    def home(self) -> str:
        directory = self.config.directory
        return (
            f'~/{directory.relative_to(cfg.USER_HOME).as_posix()}'
            if directory.is_relative_to(cfg.USER_HOME) else
            f'./{directory.relative_to(cfg.CWD).as_posix()}'
            if directory.is_relative_to(cfg.CWD) else
            directory.absolute().as_posix()
            )

    @functools.cached_property
    def urls(self) -> typing.Mapping[str, str]:
        return dict(
            item.split(', ', 1)
            for item in cfg.metadata.get_all('Project-URL')
            if ', ' in item
            )

    @functools.cached_property
    def config(self) -> cfg.Config:
        return cfg.Config.from_environment()

    @functools.cached_property
    def user_directories(self) -> tuple[tuple[pathlib.Path, str, str], ...]:
        theme = Gtk.IconTheme.get_for_display(ui.main.root.get_display())
        home = pathlib.Path.home()
        root = pathlib.Path(home.anchor)
        special_dirs = (
            (pathlib.Path(path), key[10:].replace('_', '').lower())
            for key, path in (
                (key, GLib.get_user_special_dir(value))
                for key, value in vars(GLib.UserDirectory).items()
                if key.startswith('DIRECTORY_')
                )
            if path
            )
        return (
            (home, ui.strings.directory_home, 'user-home'),
            (root, ui.strings.directory_computer, 'drive-harddisk'),
            *(
                (path, path.name, next(
                    filter(theme.has_icon, (f'folder-{slug}', f'user-{slug}')),
                    'folder',
                    ))
                for path, slug in special_dirs
                ),
            )

    @ui.app.connect('handle-local-options')
    def on_local_options(
            self,
            app: 'Application',
            options: GLib.VariantDict,
            ) -> typing.Literal[-1]:
        size = options.lookup_value('window-size', GLib.VariantType('s'))
        if size:
            w, h = map(int, size.get_string().split('x'))
            self.window_size_request = w, h
        return -1

    @ui.app.connect('startup')
    def on_startup(self, app: 'Application') -> None:
        self.initialized = True

    @ui.app.connect('activate')
    def on_activate(self, app: 'Application') -> None:
        if self.window_size_request:
            ui.main.root.set_default_size(*self.window_size_request)
        ui.main.root.present()

    @ui.app.connect('shutdown')
    def on_shutdown(self, app: 'Application') -> None:
        self.server_loop_run = False
        self.executor.shutdown()

    @ui.directory.connect('response')
    def on_directory(self, widget: Gtk.FileChooserNative, r: int) -> None:
        if r == Gtk.ResponseType.ACCEPT:
            selected = widget.get_file()
            if not selected:
                # WORKAROUND(flatpak bug): show error dialog
                # https://github.com/flatpak/xdg-desktop-portal/issues/820
                ui.directory_error.root.show()
                return
            self.set_directory(selected.get_path(), True)

    @ui.directory_error.connect('response')
    def on_messagedialog(self, widget: Gtk.Dialog, r: int) -> None:
        widget.hide()

    @ui.main.connect('help', 'activate')
    def on_help(self, widget, data):
        Gtk.show_uri(self.window, self.urls['help'], Gdk.CURRENT_TIME)

    @ui.main.connect('about', 'activate')
    def on_about(self, widget, data):
        ui.about.root.present()

    @ui.main.connect('user_reset', 'clicked', None, 'username')
    @ui.main.connect('password_reset', 'clicked', None, 'password')
    @ui.main.connect('directory_reset', 'clicked', None, 'directory')
    @ui.main.connect('port_reset', 'clicked', None, 'port')
    def on_reset(self, widget, value):
        getattr(self, f'set_{value}')(None, True)

    @ui.main.connect('directory_button', 'clicked')
    def on_directory_click(self, widget: Gtk.Button) -> None:
        ui.directory.root.show()

    @ui.main.connect('readonly_switch', 'state-set')
    def on_readonly_change(self, widget, value):
        self.set_readonly(value, self.initialized)

    @ui.main.connect('port_entry', 'value-changed')
    def on_port_change(self, widget):
        self.set_port(widget.get_value_as_int(), self.initialized)

    @ui.main.connect('protocol_switch_ftp', 'clicked', None, 0)
    @ui.main.connect('protocol_switch_ftps', 'clicked', None, 1)
    @ui.main.connect('protocol_switch_sftp', 'clicked', None, 2)
    def on_protocol_change(self, widget, value):
        self.set_protocol(value, True)

    @ui.main.connect('server_start', 'activated', None, True)
    @ui.main.connect('server_stop', 'clicked', None, False)
    def on_server_click(self, widget, value):
        self.set_online(value, True)

    def set_username(self, username: str, user: bool = False) -> None:
        entry: Gtk.Entry = ui.main.user_entry
        entry.set_text('')  # TODO(if made active): make all this conditional
        entry.get_first_child().set_placeholder_text((
            self.config.default.username
            if user or not username else
            username
            ))

        if user:
            self.config = self.config.with_updates(username=username)

    def set_password(self, password: typing.Any, user: bool = False) -> None:
        entry: Gtk.PasswordEntry = ui.main.password_entry
        entry.set_text('')  # TODO(if made active): make all this conditional
        entry.get_first_child().set_placeholder_text((
            self.config.default.password
            if user else
            ui.strings._dummy_password
            if isinstance(password, crypto.CryptoHash) else
            password
            ))

        if user:
            self.config = self.config.with_updates(password=password)

    def set_port(self, port: typing.Optional[int], user: bool = False) -> None:
        entry: Gtk.SpinButton = ui.main.port_entry

        if port is None:
            port = self.config.default.port

        if port != entry.get_value_as_int():
            entry.set_value(port)
            if user:
                return  # wait for value-changed

        if self.check_port(port) and user:
            self.config = self.config.with_updates(port=port)

    def set_directory(
            self,
            path: typing.Optional[os.PathLike],
            user: bool = False
            ) -> None:
        path = pathlib.Path(path) if path else self.config.default.directory
        icon, label = next(
            (
                (c, l)
                for p, l, c in self.user_directories
                if p.is_dir() and p.samefile(path)
                ),
            ('folder', path.name)
            )

        if user:
            self.config = self.config.with_updates(directory=path)

        ui.main.directory_content.set_icon_name(icon)
        ui.main.directory_content.set_label(label)
        ui.main.directory_button.set_tooltip_markup(
            ui.strings._render_directory_tooltip(home=self.home),
            )

    def set_protocol(self, value: int, user: bool = False) -> None:
        buttons: tuple[Gtk.ToggleButton] = (
            ui.main.protocol_switch_ftp,
            ui.main.protocol_switch_ftps,
            ui.main.protocol_switch_sftp,
            )

        if not buttons[value].get_active():
            buttons[value].set_active(True)
            if user:
                return  # wait for value-changed

        labels: tuple[Gtk.Label] = (
            ui.main.protocol_ftp_label,
            ui.main.protocol_ftps_label,
            ui.main.protocol_sftp_label,
            )
        for i, w in enumerate(labels):
            w.set_visible(i == value)

        if user:
            self.config = self.config.with_updates(secure=value)

    def set_readonly(self, readonly: bool, user: bool = False) -> None:
        switch: Gtk.Switch = ui.main.readonly_switch

        if readonly != switch.get_active():
            switch.set_active(readonly)
            if user:
                return  # wait for value-changed

        ui.main.readonly_disabled.set_reveal_child(not readonly)

        if user:
            self.config = self.config.with_updates(readonly=readonly)

    def set_online(self, state: bool, user: bool = False):
        if state:
            if not (self.port_valid and self.check_port(self.config.port)):
                ui.main.port_entry.grab_focus()
                logger.warning('Server start bounced, invalid port')
                return

            if self.server_loop_running:
                logger.warning('Server start bounced, already running')
                return

        elif not self.server_loop_run:
            logger.warning('Server stop bounced, already stopping')
            return

        pages = 'left', 'right'
        transitions = (
            Gtk.StackTransitionType.SLIDE_RIGHT,
            Gtk.StackTransitionType.SLIDE_LEFT,
            )

        ui.main.back_revealer.set_reveal_child(state)
        ui.main.stack.set_transition_type(transitions[state])
        ui.main.stack.set_visible_child_name(pages[state])

        if not state:
            text = (
                ui.strings.status_server_stopping
                if self.server_loop_running else
                None
                )
            self.set_status(None, text)
            self.server_loop_run = False
            return

        credentials = {
            key: value
            for key, value in (
                ('username', ui.main.user_entry.get_text()),
                ('password', ui.main.password_entry.get_text()),
                )
            if value
            }

        self.config = config = self.config.with_updates(
            **credentials,
            info=cfg.Info(last=datetime.datetime.now()),
            )
        config.save()

        self.set_status(None, ui.strings.status_server_starting)
        self.server_loop_run = True
        self.server_loop = self.executor.submit(self.loop_server)

    def set_status(self, icon=None, text=None, created=None):
        if created and created < self.status_time:
            return
        self.status_time = time.time()

        ui.main.status_revealer.set_reveal_child(text is not None)

        if text:
            ui.main.status_label.set_label(text)
            ui.main.status_spinner.set_visible(icon is None)
            ui.main.status_image.set_visible(icon is not None)

        if icon:
            ui.main.status_image.set_from_icon_name(icon)

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, application_id=self.application_id, **kwargs)
        self.get_style_manager().set_color_scheme(Adw.ColorScheme.PREFER_DARK)
        self.set_accels_for_action('app.help', ['F1'])
        self.add_main_option(
            'window-size',
            ord('s'),  # WTF GObject!
            0,
            GLib.OptionArg.STRING,
            'Desired window size',
            'WIDTHxHEIGHT',
            )

        self.executor = concurrent.futures.ThreadPoolExecutor()

        self.textbuffer: Gtk.TextBuffer = ui.main.textview.get_buffer()
        self.textend = self.textbuffer.get_end_iter()
        self.textmark = self.textbuffer.create_mark('', self.textend, False)

        ui.main.port_entry.set_range(srv.MIN_PORT, srv.MAX_PORT)

        self.set_username(self.config.username)
        self.set_password((
            self.config.password
            or self.config.password_hash
            or self.config.default.password
            ))
        self.set_directory(self.config.directory)
        self.set_readonly(self.config.readonly)
        self.set_port(self.config.port)
        self.set_protocol(self.config.secure)

        handler = GLibLogHandler(self.on_log_record)

        for lib_name in ('pyftpdlib', 'asyncssh'):
            lib_logger = logging.getLogger(lib_name)
            lib_logger.addHandler(handler)
            lib_logger.setLevel(logging.INFO)

        self.logger = logging.getLogger(f'{__name__}:application')
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.INFO)

    def on_log_record(self, record: logging.LogRecord) -> None:
        created = datetime.datetime.fromtimestamp(record.created)
        message = '<tt>{} {} </tt>{}\n'.format(
            created.replace(microsecond=0).isoformat(' '),
            record.levelname,
            (
                getattr(record, 'markup', None)
                or f'<b>{GLib.markup_escape_text(record.getMessage())}</b>'
                ),
            )

        textview: Gtk.TextView = ui.main.textview
        adjustment: Gtk.Adjustment = textview.get_vadjustment()
        bottomed = getattr(record, 'scroll', None) or 1 >= (
            adjustment.get_upper()
            - adjustment.get_value()
            - adjustment.get_page_size()
            - textview.get_top_margin()
            - textview.get_bottom_margin()
            )
        self.textbuffer.insert_markup(self.textend, message, -1)

        if bottomed:
            GLib.idle_add(
                textview.scroll_to_mark,
                self.textmark, 0, False, 0, 0,
                )

    def check_port(self, port: int) -> bool:
        check = functools.partial(srv.check_port, port)
        self.port_valid = valid = all(self.executor.map(check, (True, False)))

        box: Gtk.Box = ui.main.port_box
        (box.remove_css_class if valid else box.add_css_class)('error')

        if not valid:
            last, expiration = self.last_port_wait
            ui.main.port_error.set_label((
                ui.strings.port_waiting_warning
                if port == last and expiration > time.time() else
                ui.strings.port_unavailable_warning
                ))

        ui.main.port_error_revealer.set_reveal_child(not valid)
        return valid

    def loop_server(self) -> None:
        try:
            server = self.server_class(self.config)
            host_lookup = self.executor.submit(srv.route_host)
            server.connect(self.server_loop_interval)
            username = self.config.auth.username
            hosts = host_lookup.result()
            port = self.config.port
            message = ui.strings.render_server_log_info(
                config=self.config,
                home=self.home,
                addresses=[
                    f'{protocol}://{username}@{host}:{port}'
                    for protocol in self.config.protocols
                    for host in (f'[{h}]' if ':' in h else h for h in hosts)
                    ],
                )
            self.logger.info(
                RE_MARKUP.sub('', message),
                extra={'markup': message, 'scroll': True},
                )
            GLib.idle_add(
                self.set_status,
                'network-idle',
                ui.strings.status_server_started,
                )
            server.run(
                lambda: self.server_loop_run,
                self.server_loop_interval,
                )
            self.port_wait = port, time.time() + srv.time_wait()
        except Exception as e:
            self.logger.exception(e)

        self.logger.info(ui.strings.server_log_stopped)
        GLib.idle_add(self.set_status)
