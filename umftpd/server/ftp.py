"""FTP server classes and network utilities."""

import functools
import typing

import pyftpdlib.authorizers
import pyftpdlib.handlers
import pyftpdlib.servers

import umftpd.config as cfg
import umftpd.server as srv


BANNER = '{}-{} ready.'.format(
    cfg.metadata['name'],
    cfg.metadata['version'],
    )


class Permissions:

    CWD = 'e'
    CDUP = CWD
    LIST = 'l'
    NLST = LIST
    STAT = LIST
    MLSD = LIST
    MLST = LIST
    SIZE = LIST
    RETR = 'r'
    APPE = 'a'
    DELE = 'd'
    RMD = DELE
    RNFR = 'f'
    RNTO = RNFR
    MKD = 'm'
    STOR = 'w'
    STOU = STOR
    SITE_CHMOD = 'M'
    SITE_MFMT = 'T'

    R = CWD + LIST + RETR
    A = APPE + MKD + STOR + SITE_MFMT
    W = A + DELE + RNFR + SITE_CHMOD
    RW = R + W


class Authorizer(pyftpdlib.authorizers.DummyAuthorizer):

    def __init__(self, config: cfg.Config):
        super().__init__()

        self.auth = config.auth
        self.add_user(
            config.auth.username,
            '',
            config.auth.home,
            perm=Permissions.R if config.readonly else Permissions.RW,
            )

    def validate_authentication(self, username, password, handler):
        if username in self.user_table and self.auth(username, password):
            return
        raise pyftpdlib.authorizers.AuthenticationFailed


class EventsMixing:

    def on_login(self, username):
        return super().on_login(username)

    def on_logout(self, username):
        return super().on_logout(username)

    def on_file_sent(self, file):
        return super().on_file_sent(file)

    def on_file_received(self, file):
        return super().on_file_received(file)

    def on_incomplete_file_sent(self, file):
        return super().on_incomplete_file_sent(file)

    def on_incomplete_file_received(self, file):
        return super().on_incomplete_file_received(file)


class FTPHandler(EventsMixing, pyftpdlib.handlers.FTPHandler):

    banner = BANNER

    @classmethod
    @functools.lru_cache(1)
    def from_config(cls, config: cfg.Config) -> typing.Type['FTPHandler']:
        """Initialize subclass from user options."""
        return type(f'Initialized{cls.__name__}', (cls,), {
            'authorizer': Authorizer(config),
            })


class FTPSHandler(EventsMixing, pyftpdlib.handlers.TLS_FTPHandler):

    banner = BANNER

    @classmethod
    @functools.lru_cache(1)
    def from_config(cls, config: cfg.Config) -> typing.Type['FTPSHandler']:
        """Initialize subclass from user options."""
        return type(f'Initialized{cls.__name__}', (cls,), {
            'authorizer': Authorizer(config),
            'keyfile': str(config.ssl.keyfile),
            'certfile': str(config.ssl.certfile),
            })


class FTPServer(srv.Server):

    backend: pyftpdlib.servers.FTPServer

    def __init__(self, config: cfg.Config) -> None:
        super().__init__(config)
        handler_cls = FTPSHandler if config.secure else FTPHandler
        self.backend = pyftpdlib.servers.FTPServer(
            ('', config.port),
            handler_cls.from_config(config),
            )

    def connect(self, interval: float) -> None:
        self.backend.serve_forever(interval, False, False)

    def run(self, live: typing.Callable[[], bool], interval: float) -> None:
        with self._context():
            while live():
                self.backend.ioloop.loop(interval, False)
        self.backend.close_all()
