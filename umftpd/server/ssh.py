"""SSH server classes."""

import asyncio
import contextlib
import errno
import functools
import typing

import asyncssh
import asyncssh.connection

import umftpd.config as cfg
import umftpd.server as srv


class SSHServer(asyncssh.SSHServer):

    config: cfg.Config

    def __init__(self, config: cfg.Config) -> None:
        self.config = config

    def validate_host_based_user(self, *_, **__) -> typing.Literal[True]:
        return True

    def password_auth_supported(self) -> typing.Literal[True]:
        return True

    def begin_auth(self, username: str) -> bool:
        return self.config.auth.username == username

    def validate_password(self, username: str, password: str) -> bool:
        return self.config.auth(username, password)


class ReadonlySFTPServer(asyncssh.SFTPServer):
    empty_attrs = asyncssh.SFTPAttrs()
    allowed_pflags = asyncssh.FXF_READ
    allowed_desired_access = (
        asyncssh.ACE4_READ_DATA
        | asyncssh.ACE4_READ_ATTRIBUTES
        )
    allowed_flags = (
        asyncssh.FXF_OPEN_EXISTING
        | asyncssh.FXF_BLOCK_READ
        | asyncssh.FXF_BLOCK_ADVISORY
        | asyncssh.FXF_NOFOLLOW
        | asyncssh.FXF_ACCESS_AUDIT_ALARM_INFO
        | asyncssh.FXF_ACCESS_BACKUP
        | asyncssh.FXF_BACKUP_STREAM
        )

    def _readonly(
            self,
            filename: typing.Union[bytes, str, None] = None,
            filename2: typing.Union[bytes, str, None] = None,
            ) -> typing.NoReturn:
        msg = 'Read-only file system'
        raise PermissionError(errno.EROFS, msg, filename, None, filename2)

    def _readonly_file(self, file_obj, *_, **__) -> typing.NoReturn:
        self._readonly(getattr(file_obj, 'name', None))

    def _readonly_path(self, path: bytes, *_, **__) -> typing.NoReturn:
        self._readonly(path)

    def _readonly_move(
            self,
            oldpath: bytes,
            newpath: bytes,
            ) -> typing.NoReturn:
        self._readonly(oldpath, newpath)

    def open(
            self,
            path: bytes,
            pflags: int,
            attrs: typing.Any,
            ) -> typing.Any:
        if pflags & ~self.allowed_pflags:
            self._readonly(path)
        return super().open(path, pflags, self.empty_attrs)

    def open56(
            self,
            path: bytes,
            desired_access: int,
            flags: int,
            attrs: typing.Any,
            ) -> typing.Any:
        if desired_access & ~self.allowed_desired_access:
            self._readonly(path)
        flags = flags & self.allowed_flags
        return super().open56(path, desired_access, flags, self.empty_attrs)

    write = _readonly_file
    setstat = _readonly_path
    fsetstat = _readonly_file
    remove = _readonly_path
    mkdir = _readonly_path
    rmdir = _readonly_path
    rename = _readonly_move
    symlink = _readonly_move
    link = _readonly_move
    lock = _readonly_file
    unlock = _readonly_file
    posix_rename = _readonly_move
    fsync = _readonly_file


class SFTPServer(srv.Server):

    task: typing.Optional[asyncio.Task[asyncssh.connection.SSHAcceptor]]

    def connect(self, interval: float) -> None:

        async def server():
            return await asyncssh.listen(
                '',
                self.config.port,
                allow_scp=True,
                reuse_address=True,
                server_factory=functools.partial(SSHServer, self.config),
                server_host_certs=[str(self.config.ssl.certfile)],
                server_host_keys=[str(self.config.ssl.keyfile)],
                sftp_factory=functools.partial(
                    (
                        ReadonlySFTPServer
                        if self.config.readonly else
                        asyncssh.SFTPServer
                        ),
                    chroot=self.config.auth.home
                    ),
                )

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        self.task = loop.create_task(server())
        loop.run_until_complete(asyncio.wait_for(
            asyncio.shield(self.task),
            timeout=interval,
            ))

    def run(self, live: typing.Callable[[], bool], interval: float) -> None:

        async def wait():
            while live():
                await asyncio.sleep(interval)
            self.task.cancel()
            connection = await self.task
            connection.close()
            await connection.wait_closed()

        loop = asyncio.get_event_loop()
        with self._context():
            loop.run_until_complete(wait())

        while tasks := asyncio.all_tasks(loop):
            with contextlib.suppress(asyncio.TimeoutError):
                loop.run_until_complete(asyncio.wait_for(
                    asyncio.gather(*tasks),
                    timeout=interval,
                    ))

        loop.close()
        asyncio.set_event_loop(None)
