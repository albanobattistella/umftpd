
.PHONY: flatpak tools wheel messages

all: wheel flatpak messages

env:
	python3 -m venv env
	env/bin/pip install -e '.[dev]'

clean:
	rm -Rf env dist build .flatpak-builder umftpd.egg-info
	find umftpd \
		-type f -name '*.py[co]' -delete \
		-o -type d -name __pycache__ -delete

flatpak:
	rm -Rf .flatpak-builder/build/
	mkdir -p dist/flatpak/
	flatpak-builder --repo=build/flatpak-repo --force-clean build/flatpak eu.ithz.umftpd.yaml
	flatpak build-bundle build/flatpak-repo dist/eu.ithz.umftpd.flatpak eu.ithz.umftpd

flatpak/install: flatpak
	flatpak install --user dist/eu.ithz.umftpd.flatpak

wheel: env
	mkdir -p dist build/wheel/
	env/bin/pip wheel -w build/wheel .
	rm -f build/wheel/PyGObject-*.whl build/wheel/pycairo-*.whl  # conflicts with runtime
	cp -f build/wheel/umftpd-*.whl dist/

messages: env
	mkdir -p build
	env/bin/python -m umftpd.uitk --pretty messages umftpd > dist/messages.json

templates: templates/flatpak

templates/flatpak: env
	cat flatpak/eu.ithz.umftpd.desktop.mustache \
	| LANGUAGE=C env/bin/python -m umftpd.uitk render \
	> flatpak/app/share/applications/eu.ithz.umftpd.desktop
	cat flatpak/eu.ithz.umftpd.metainfo.xml.mustache \
	| LANGUAGE=C env/bin/python -m umftpd.uitk render \
	> flatpak/app/share/metainfo/eu.ithz.umftpd.metainfo.xml

demo: env
	LANGUAGE=C env/bin/python -m umftpd.uitk demo umftpd

tools: tools/flatpak
tools/flatpak: tools/flatpak-pip-generator

tools/flatpak-pip-generator:
	mkdir -p tools/
	wget -O tools/flatpak-pip-generator https://raw.githubusercontent.com/flatpak/flatpak-builder-tools/master/pip/flatpak-pip-generator
	chmod +x tools/flatpak-pip-generator
